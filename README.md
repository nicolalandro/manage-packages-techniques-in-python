# Manage Packages techniques in python
Explanation about technique to manage

## Venv
```
python3.8 -m venv vnev

source venv/bin/activate
# on windows
source venv/bin/activate.bat
# on fish shell
source venv/bin/activate.fish

pip install <lib>

pip freeze > requirements.txt

deactivate
```

## Pipenv

```
pipenv --python 3.8
pipenv install --dev <lib>

# Install a local setup.py into your virtual environment/Pipfile:
pipenv install -e .

# Use a lower-level pip command:
pipenv run pip/jupyter...

```

## Conda

```
conda create --name <name_env>
conda activate <env_name>

conda install <LIB>

conda deactivate
```

## Poetry

```
# docker run -it fnndsc/python-poetry /bin/bash
poetry init
# -> pyproject.toml
poetry add <LIB>

poetry run pip/jupyter...

# poetry build, publish
# poetry show --tree
```

# Docker

```
docker-compose up

# localhost:8888 jupyter
# localhost:8000 # served slide
# get pdf http://localhost:8000/Slide.slides.html?print-pdf
```

# References
* python
* pip
* [venv](https://virtualenv.pypa.io/en/latest/1)
* [pipenv](https://github.com/pypa/pipenv)
* [conda](https://docs.conda.io/projects/conda/en/latest/commands.html)
* [poetry](https://python-poetry.org/)