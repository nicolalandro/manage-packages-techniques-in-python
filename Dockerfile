FROM jupyter/scipy-notebook

RUN pip install poetry pipenv
RUN conda install -c conda-forge rise
RUN conda init bash

WORKDIR /home/test

RUN pip install --upgrade pip

CMD jupyter notebook --allow-root --ip "0.0.0.0"